const express = require('express')
const app = express()
const port = process.env.PORT || 3000
const bodyParser = require('body-parser')
const capture = require('./src/capture')

app.use(bodyParser.json())

app.post('/', (req, res) => {

    console.log(`Recebendo notificacao de pagamento: ${JSON.stringify(req.body)}`)

    // colocar em uma fila para capturar
    setTimeout(async () => {
        try {
            await capture(req.body.id)
            console.log(`Pagamento capturado ${req.body.id}`)
        } catch(e) {
            console.log(`Erro ao capturar o pagamento ${req.body.id}`)
            console.error(e)
        }
    }, 1000)

    res.send('ok')
})

app.listen(port, () => {
    console.log(`App listening at http://localhost:${port}`)
})
