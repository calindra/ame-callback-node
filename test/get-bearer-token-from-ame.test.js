const getBearerTokenFromAme = require("../src/get-bearer-token-from-ame")

describe("get bearer token from Ame", () => {
    it("should do the auth", async () => {
        let clientId = process.env.CLIENT_ID
        let clientSecret = process.env.CLIENT_SECRET
        let token = await getBearerTokenFromAme(clientId, clientSecret)
        expect(token.split(/\./)[1]).toBeDefined()
    })
})