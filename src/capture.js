const getBearerTokenFromAme = require("./get-bearer-token-from-ame")
const axios = require('axios')

module.exports = async function capture(paymentId) {
    let clientId = process.env.CLIENT_ID
    let clientSecret = process.env.CLIENT_SECRET
    let token = await getBearerTokenFromAme(clientId, clientSecret)
    let url = `https://${process.env.AME_HOST}/api/wallet/user/payments/${paymentId}/capture`

    await axios.put(url, {}, {
        headers: {
            Authorization: `Bearer ${token}`
        }
    })
}
