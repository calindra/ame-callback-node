const axios = require('axios')
const FormData = require('form-data')

module.exports = async function getBearerTokenFromAme(clientId, clientSecret) {
    if (!clientId) throw new Error(`clientId is required`)
    if (!clientSecret) throw new Error(`clientSecret is required`)
    if (!process.env.AME_HOST) throw new Error(`Environment variable AME_HOST is missing`)
    
    const AME_AUTH = `https://${process.env.AME_HOST}/api/auth/oauth/token`

    const data = new FormData()
    data.append('grant_type', 'client_credentials')

    const config = {
        method: 'post',
        url: AME_AUTH,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            ...data.getHeaders(),
        },
        auth: {
            username: clientId,
            password: clientSecret,
        },
        data: data,
    }
    try {
        let response = await axios(config)
        return response.data.access_token
    } catch (e) {
        console.log(
            `Erro ao se autenticar na Ame \n\turl: ${AME_AUTH}\n\tclientId: ...${(
                clientId || ''
            ).substr(-3)}\n\tclientSecret: ...${(clientSecret || '').substr(
                -3
            )}`
        )
        throw e
    }
}
